import React from 'react'
import { AxiosError } from 'axios'

import ApplicationApi from '../../api/ApplicationApi'
import Button from '../UI/Button'
import Input from '../UI/Input'
import InputSecond from '../UI/InputSecond'
import Reg from '../../utils/reg'

import classes from './style.module.scss'

const INIT_PRICE_AUTO = 1000000
const MAX_PRICE_AUTO = 6000000
const INIT_FIRST_PAYMENT = 10
const MAX_FIRST_PAYMENT = 60
const INIT_MONTHS_LEASING = 1
const MAX_MONTHS_LEASING = 60
const INTEREST_RATE = 0.035

const FormCalculationLeasing = () => {
   const [car_coast, set_car_coast] = React.useState(INIT_PRICE_AUTO)
   const [initail_payment_percent, set_initail_payment_percent] =
      React.useState(INIT_FIRST_PAYMENT)
   const [lease_term, set_lease_term] = React.useState(INIT_MONTHS_LEASING)
   const [loading, setLoading] = React.useState(false)

   const initail_payment = Math.floor((initail_payment_percent / 100) * car_coast)

   const monthly_payment_from = Math.floor(
      (car_coast - initail_payment) *
         ((INTEREST_RATE * (1 + INTEREST_RATE) ** lease_term) /
            ((1 + INTEREST_RATE) ** lease_term - 1))
   )

   const total_sum = Math.floor(
      car_coast * (initail_payment_percent / 100) + lease_term * monthly_payment_from
   )

   const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
      try {
         event.preventDefault()
         setLoading(true)
         await sendData()
         
      } catch (error) {
         const err = error as AxiosError
         console.log(err.response?.data)
      } finally {
         setLoading(false)
      }
   }

   const sendData = async () => {
      const res = await ApplicationApi.send({
         car_coast,
         initail_payment,
         initail_payment_percent,
         lease_term,
         total_sum,
         monthly_payment_from,
      })
      alert(JSON.stringify(res.data))
   }

   return (
      <form className={classes['OxemForm']} onSubmit={handleSubmit}>
         <h1 className={classes['OxemForm-title']}>
            Рассчитайте стоимость автомобиля в лизинг
         </h1>
         <div className={classes['OxemForm-containerItems']}>
            <div className={classes['containerItems-item']}>
               <h6 className={classes['item-title']}>Стоимость автомобиля</h6>
               <Input
                  disabled={loading}
                  postfix={'₽'}
                  min={INIT_PRICE_AUTO}
                  max={MAX_PRICE_AUTO}
                  value={car_coast}
                  onChange={event => set_car_coast(+event.target.value)}
               />
            </div>
            <div className={classes['containerItems-item']}>
               <h6 className={classes['item-title']}>Первоначальный взнос</h6>
               <InputSecond
                  disabled={loading}
                  min={INIT_FIRST_PAYMENT}
                  max={MAX_FIRST_PAYMENT}
                  prefix={initail_payment}
                  value={initail_payment_percent}
                  onChange={event => set_initail_payment_percent(+event.target.value)}
               />
            </div>
            <div className={classes['containerItems-item']}>
               <h6 className={classes['item-title']}>Срок лизинга</h6>
               <Input
                  disabled={loading}
                  min={INIT_MONTHS_LEASING}
                  max={MAX_MONTHS_LEASING}
                  postfix={'мес.'}
                  value={lease_term}
                  onChange={event => set_lease_term(+event.target.value)}
               />
            </div>
         </div>
         <div className={classes['OxemForm-buttom']}>
            <div className={classes['OxemForm-containerOuts']}>
               <div className={classes['containerOuts-item']}>
                  <h6 className={classes['item-title']}>Сумма договора лизинга</h6>
                  <h1 className={classes['item-content']}>
                     {Reg.money(total_sum)}
                  </h1>
               </div>
               <div className={classes['containerOuts-item']}>
                  <h6 className={classes['item-title']}>Ежемесячный платеж от</h6>
                  <h1 className={classes['item-content']}>{Reg.money(monthly_payment_from)}</h1>
               </div>
            </div>
            <div className={classes['buttom-btn']}>
               <Button loading={loading}>Оставить заявку</Button>
            </div>
         </div>
      </form>
   )
}

export default FormCalculationLeasing
