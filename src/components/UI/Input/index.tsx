import React from 'react'
import cn from 'classnames'
import cnBind from 'classnames/bind'

import classes from './style.module.scss'

const cx = cnBind.bind(classes)

interface IProps {
   postfix?: React.ReactNode
   min?: number
   max?: number
   placeholder?: string
   disabled?: boolean
   value?: number
   onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const Input: React.FC<IProps> = ({
   postfix,
   min,
   max,
   placeholder,
   disabled,
   value,
   onChange,
}) => {
   const refRange = React.useRef<HTMLInputElement>(null)
   let refTimeout = React.useRef<NodeJS.Timeout | undefined>(undefined)

   const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      if (!onChange) return
      event.target.value = event.target.value.replace(/\D/g, '')

      clearTimeout(refTimeout.current)

      if (min && Number(event.target.value) < min) {
         refTimeout.current = setTimeout(() => {
            event.target.value = String(min)
            onChange(event)
         }, 750)
      }
      if (max && Number(event.target.value) > max) {
         refTimeout.current = setTimeout(() => {
            event.target.value = String(max)
            onChange(event)
         }, 750)
      }

      onChange(event)
   }

   const rangeProgress = React.useMemo(() => {
      const val = Number(value || refRange.current?.value)

      if (min && max && val) {
         const result = ((val - min) * 100) / (max - min)
         if (result < 0) {
            return 0 + '%'
         }
         if (result > 100) {
            return 100 + '%'
         }
         return result + '%'
      }
   }, [value])

   return (
      <div className={cn(classes['OxemInput-wrappper'], cx({ disabled }))}>
         <div className={classes['OxemInput-content']}>
            <input
               className={classes['OxemInput-input']}
               type='text'
               placeholder={placeholder}
               disabled={disabled}
               value={value?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')}
               onChange={handleChange}
            />
            {postfix && <div className={classes['OxemInput-adornment']}>{postfix}</div>}
         </div>
         <div className={cn(classes['OxemInput-wrapperRange'], cx({ disabled }))}>
            <input
               ref={refRange}
               className={classes['OxemInput-range']}
               disabled={disabled}
               type='range'
               min={min}
               max={max}
               value={value}
               onChange={handleChange}
            />
            <div
               className={classes['OxemInput-rangeProgress']}
               style={{ width: rangeProgress }}
            />
         </div>
      </div>
   )
}

export default Input
