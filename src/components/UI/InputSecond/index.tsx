import React from 'react'
import cn from 'classnames'
import cnBind from 'classnames/bind'

import classes from './style.module.scss'

const cx = cnBind.bind(classes)

interface IProps {
   prefix?: number
   min?: number
   max?: number
   placeholder?: string
   disabled?: boolean
   value?: number
   onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const InputSecond: React.FC<IProps> = ({
   prefix,
   min,
   max,
   placeholder,
   disabled,
   value,
   onChange,
}) => {
   const refRange = React.useRef<HTMLInputElement>(null)
   let refTimeout = React.useRef<NodeJS.Timeout | undefined>(undefined)

   const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      if (!onChange) return
      event.target.value = event.target.value.replace(/\D/g, '')

      clearTimeout(refTimeout.current)

      if (min && Number(event.target.value) < min) {
         refTimeout.current = setTimeout(() => {
            event.target.value = String(min)
            onChange(event)
         }, 750)
      }
      if (max && Number(event.target.value) > max) {
         refTimeout.current = setTimeout(() => {
            event.target.value = String(max)
            onChange(event)
         }, 750)
      }

      onChange(event)
   }

   const rangeProgress = React.useMemo(() => {
      const val = Number(value || refRange.current?.value)

      if (min && max && val) {
         const result = ((val - min) * 100) / (max - min)
         if (result < 0) {
            return 0 + '%'
         }
         if (result > 100) {
            return 100 + '%'
         }
         return result + '%'
      }
   }, [value])

   return (
      <div className={cn(classes['OxemInputSecond-wrappper'], cx({ disabled }))}>
         <label className={classes['OxemInputSecond-content']}>
            <div className={classes['OxemInputSecond-prefix']}>
               {prefix?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + ' ₽'} 
            </div>
            <div className={classes['OxemInputSecond-inputWrapper']}>
               <input
                  className={classes['OxemInputSecond-input']}
                  type='text'
                  placeholder={placeholder}
                  disabled={disabled}
                  value={value + '%'}
                  onChange={handleChange}
               />
            </div>
         </label>
         <div className={cn(classes['OxemInputSecond-wrapperRange'], cx({ disabled }))}>
            <input
               ref={refRange}
               className={classes['OxemInputSecond-range']}
               disabled={disabled}
               type='range'
               min={min}
               max={max}
               value={value}
               onChange={handleChange}
            />
            <div
               className={classes['OxemInputSecond-rangeProgress']}
               style={{ width: rangeProgress }}
            />
         </div>
      </div>
   )
}

export default InputSecond
