import React from 'react'
import cn from 'classnames'
import cnBind from 'classnames/bind'

import classes from './style.module.scss'

const cx = cnBind.bind(classes)

interface IProps {
   loading?: boolean
   children?: string
   disabled?: boolean
}

const Button: React.FC<IProps> = ({ loading, children, disabled }) => {
   const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      if (loading) {
         event.preventDefault()
         return
      }
   }

   return (
      <button
         className={cn(classes['OxemButton-wrappper'], cx({ loading }))}
         disabled={disabled}
         onClick={handleClick}
      >
         {loading && <div className={classes['OxemButton-loader']} />}
         {children}
      </button>
   )
}

export default Button
