import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Home from '../pages/Home'
import Presentation from '../pages/Presentation'

const AppRouter = () => {
   return (
      <Routes>
         <Route path={'/'} element={<Home />} />
         <Route path={'/presentation'} element={<Presentation />} />
      </Routes>
   )
}

export default AppRouter
