import React from 'react'

import FormCalculationLeasing from '../../components/FormCalculationLeasing'

import classes from './style.module.scss'

const Home = () => {
   return (
      <div className={classes['OxemHome-wrapper']}>
         <FormCalculationLeasing />
      </div>
   )
}

export default Home
