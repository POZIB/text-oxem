import React from 'react'

import Button from '../../components/UI/Button'
import Input from '../../components/UI/Input'

import classes from './style.module.scss'

const Presentation = () => {
   const [value1, setValue1] = React.useState(2000)
   const [value2, setValue2] = React.useState(10000)

   return (
      <div className={classes['OxemPresentation-wrapper']}>
         <div className={classes['Container-buttons']}>
            <div className={classes['Button-container']}>
               <span>Passive</span>
               <Button>Passive</Button>
            </div>
            <div className={classes['Button-container']}>
               <span>Disabled</span>
               <Button disabled>Disabled</Button>
            </div>
            <div className={classes['Button-container']}>
               <span>Loading</span>
               <Button loading>Loading</Button>
            </div>
            <div className={classes['Button-container']}>
               <span>Disabled and Loading</span>
               <Button disabled loading>
                  Disabled
               </Button>
            </div>
         </div>

         <div className={classes['Container-Inputs']}>
            <div className={classes['Inputs-Container']}>
               <span>Passive</span>
               <Input
                  postfix={'Passive'}
                  min={500}
                  max={5000}
                  value={value1}
                  onChange={event => setValue1(+event.target.value)}
               />
            </div>
            <div className={classes['Inputs-Container']}>
               <span>Disabled</span>
               <Input
                  disabled
                  postfix={'Disabled'}
                  min={2000}
                  max={50000}
                  value={value2}
                  onChange={event => setValue2(+event.target.value)}
               />
            </div>
         </div>
      </div>
   )
}

export default Presentation
