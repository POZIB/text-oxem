export default class Reg {
   static money(data: string | number) {
      return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + ' ₽'
   }
}
