import axios, { AxiosResponse } from 'axios'

interface data {
   car_coast: number
   initail_payment: number
   initail_payment_percent: number,
   lease_term: number
   total_sum: number
   monthly_payment_from: number
}

export default class ApplicationApi {
   static async send(data: data): Promise<AxiosResponse<{success: string}>> {
      const res = await axios.post<{success: string}>(
         'https://hookb.in/eK160jgYJ6UlaRPldJ1P',
         { data }, //axios автоматически переводит js объекты в формат JSON
       { headers: { 
                  "Content-Type": "application/json"}
               }
            )
      return res
   }
}
